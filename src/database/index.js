import mongoose from 'mongoose';

export const conn = (mongoURI) => {
  mongoose.set('useCreateIndex', true);
  mongoose.connect(mongoURI, { useNewUrlParser: true, useUnifiedTopology: true });
  mongoose.Promise = global.Promise;
};

export default mongoose;
