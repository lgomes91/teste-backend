export const admin = (req, res, next) => {
  try {
    const { user } = req.body;

    if (!user || user.role !== 'admin') {
      res.status(403).send();
    }

    next();
  } catch (error) {
    res.status(500).send('Internal Server Error');
  }
};
