import { decodeToken } from '../utils/token.utils';
import User from '../models/user.model';

export const auth = async (req, res, next) => {
  try {
    if (!req.headers.authorization) {
      return res.status(400).send('Missing token');
    }

    const authorization = req.headers.authorization.split(' ');

    if (!authorization) {
      return res.status(403).send();
    }

    const token = authorization[1];

    const decodedToken = decodeToken(token);

    if (!decodedToken) {
      return res.status(403).send();
    }
    const user = await User.findOne({ _id: decodedToken.id }).select({ isActive: 1 });

    if (!user) {
      return res.status(404).send('User not found');
    }
    if (!user.isActive) {
      return res.status(403).send('User deleted, asks an administrator do get back your account');
    }

    req.body.user = {
      name: decodedToken.name,
      id: decodedToken.id,
      role: decodedToken.role,
    };

    next();
  } catch (error) {
    res.status(500).send('Internal Server Error');
  }
};
