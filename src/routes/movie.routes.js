import { Router } from 'express';
import { auth } from '../middlewares/auth.middleware';
import { admin } from '../middlewares/admin.middleware';
import { userOnly } from '../middlewares/user.middleware';
import {
  createMovie,
  update,
  list,
  listById,
  vote,
  inactivate,
} from '../controllers/movie.controller';

export const movieRouter = Router();
/** @allowed_to_all */
movieRouter.get('/', auth, list);

movieRouter.get('/:id', auth, listById);

/** @allowed_only_to_user */
movieRouter.post('/vote', auth, userOnly, vote);

/** @allowed_only_to_admin */
movieRouter.post('/', auth, admin, createMovie);

movieRouter.put('/:id', auth, admin, update);

movieRouter.delete('/:id', auth, admin, inactivate);
