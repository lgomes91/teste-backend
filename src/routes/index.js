import { Router } from 'express';
import { userRouter } from './user.routes';
import { movieRouter } from './movie.routes';

const router = new Router();

router.use('/users', userRouter);
router.use('/movies', movieRouter);

export default router;
