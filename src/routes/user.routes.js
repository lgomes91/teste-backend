import { Router } from 'express';
import { auth } from '../middlewares/auth.middleware';
import { admin } from '../middlewares/admin.middleware';
import {
  createUser,
  session,
  updatePicture,
  update,
  listById,
  list,
  updateMe,
  me,
  inactivate,
  activate,
} from '../controllers/user.controller';

export const userRouter = Router();

/** @allowed_to_all */
userRouter.post('/', createUser);

userRouter.post('/session', session);

userRouter.post('/updatePicture', auth, updatePicture);

userRouter.get('/me', auth, me);

userRouter.put('/me', auth, updateMe);

/** @admin_routes */
userRouter.get('/', auth, admin, list);

userRouter.get('/:id', auth, admin, listById);

userRouter.put('/:id', auth, admin, update);

userRouter.delete('/:id', auth, admin, inactivate);

userRouter.patch('/:id', auth, admin, activate);
