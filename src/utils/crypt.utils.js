import { compareSync, hashSync } from 'bcrypt';

export const crypt = (word) => {
  try {
    const salt = process.env.SALT;

    if (!salt || !word) {
      return undefined;
    }

    const hashed = hashSync(word, Number(salt));

    return hashed;
  } catch (error) {
    console.log(error);
    return undefined;
  }
};

export const verify = (word, encrypt) => {
  try {
    if (!word || !encrypt) {
      return new Error('Failed to hash password');
    }

    return compareSync(word, encrypt);
  } catch (error) {
    return undefined;
  }
};
