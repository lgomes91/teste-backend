import jsonwebtoken from 'jsonwebtoken';

export const createToken = (id, name, role) => {
  try {
    if (!id || !name || !role) {
      return undefined;
    }

    const secret = process.env.SECRET;
    if (!secret) {
      return undefined;
    }

    const token = jsonwebtoken.sign({ id, name, role }, secret, {
      expiresIn: 86400,
    });

    return token;
  } catch (error) {
    return undefined;
  }
};

export const decodeToken = (token) => {
  try {
    if (!token) {
      return undefined;
    }

    const secret = process.env.SECRET;

    if (!secret) {
      return undefined;
    }

    const decodedToken = jsonwebtoken.verify(token, secret);

    if (!decodedToken) {
      return undefined;
    }

    return decodedToken;
  } catch (error) {
    return undefined;
  }
};
