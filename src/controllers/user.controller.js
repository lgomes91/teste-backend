/* eslint-disable no-await-in-loop */
/* eslint-disable max-len */
import User from '../models/user.model';
import Movie from '../models/movie.model';
import { crypt, verify } from '../utils/crypt.utils';
import { createToken } from '../utils/token.utils';

export const createUser = async (req, res) => {
  try {
    const {
      email, name, password, adminToken,
    } = req.body;

    if (!email || !name || !password) {
      return res.status(400).send('Missing resources');
    }

    if (password.length < 6) {
      return res.status(400).send('Password must have at least 6 characters');
    }

    const checkEmail = await User.find({ email }).countDocuments();

    if (checkEmail > 0) {
      return res.status(400).send('Email already used');
    }

    const hashed = crypt(password);

    if (!hashed) {
      return res.status(400).send('Failed to create user');
    }

    const user = await User.create({
      email,
      name,
      password: hashed,
      vote: [],
      role: adminToken === process.env.ADMIN_TOKEN ? 'admin' : 'user',
    });

    res.send({
      id: user.id,
      name: user.name,
      email: user.email,
      role: user.role,
    });
  } catch (error) {
    res.status(500).send('Internal Server Error');
  }
};

export const session = async (req, res) => {
  try {
    const { email, password } = req.body;

    if (!email || !password) {
      return res.status(400).send('Missing Resources');
    }

    const user = await User.findOne({ email });

    if (!user || !verify(password, user.password)) {
      return res.status(400).send('Invalid credentials');
    }

    const token = createToken(user._id, user.name, user.role);

    if (!token) {
      return res.status(400).send('Faield to generate token');
    }

    res.send({ token, name: user.name });
  } catch (error) {
    res.status(500).send('Internal Server Error');
  }
};

export const updatePicture = async (req, res) => {
  try {
    const { picture, user } = req.body;

    if (!picture || !user) {
      return res.status(400).send('Missing resources');
    }

    const updatePic = await User.updateOne({ _id: user.id }, { picture });

    if (updatePic.n === 0) {
      return res.status(400).send('Faield to update picture');
    }

    res.send();
  } catch (error) {
    res.status(500).send('Internal Server Error');
  }
};

export const listById = async (req, res) => {
  try {
    const { id } = req.params;

    if (!id) {
      return res.status(400).send('Missing resources');
    }

    const user = await User.findOne({ _id: id }).select({
      email: 1, name: 1, role: 1, picture: 1,
    });

    if (!user) {
      return res.status(404).send('User not found');
    }

    return res.send(user);
  } catch (error) {
    res.status(500).send('Internal Server Error');
  }
};

export const list = async (req, res) => {
  try {
    // console.log(req);
    if (!req.query.limit || !req.query.page) {
      return res.status(400).send('Missing resources');
    }

    const limit = Number(req.query.limit);
    const page = Number(req.query.page) - 1;

    const { name } = req.query;

    const query = {};

    if (name) {
      query.name = { $regex: name, $options: 'si' };
    }

    console.log(query);

    const users = await User.find(query)
      .skip(limit * page)
      .limit(limit)
      .select({
        email: 1, name: 1, role: 1, picture: 1,
      })
      .sort({ name: 1 });

    const totalUsers = await User.find(query).countDocuments();

    const totalPages = (totalUsers % limit) > 0
      ? Math.trunc(totalUsers / limit) + 1
      : Math.trunc(totalUsers / limit);

    return res.send({ users, totalUsers, totalPages });
  } catch (error) {
    res.status(500).send('Internal Server Error');
  }
};

export const update = async (req, res) => {
  try {
    const { id } = req.params;

    const {
      email, name, role, picture,
    } = req.body;

    console.log({
      email, name, role, picture, id,
    });

    if (!id || !email || !name || !role || (!picture && picture !== null)) {
      return res.status(400).send('Missing resources');
    }

    const checkEmail = await User.findOne({ email });

    if (checkEmail && checkEmail._id.toString() !== id) {
      return res.status(400).send('Email already in use');
    }

    const user = await User.updateOne({ _id: id }, {
      email, name, role, picture,
    });

    if (user.nModified !== 1) {
      return res.status(400).send('Update failed');
    }

    res.send();
  } catch (error) {
    res.status(500).send('Internal Server Error');
  }
};

export const me = async (req, res) => {
  try {
    const { id } = req.body.user;

    if (!id) {
      res.status(400).send('Missing resources');
    }

    const user = await User.findOne({ _id: id }).select({
      email: 1, name: 1, role: 1, picture: 1,
    });

    if (!user) {
      return res.send(400).status('User not found');
    }

    res.send(user);
  } catch (error) {
    res.status(500).send('Internal Server Error');
  }
};

export const updateMe = async (req, res) => {
  try {
    const { id } = req.body.user;

    const {
      email, name, role, picture,
    } = req.body;

    if (!id || !email || !name || !role || !picture) {
      return res.status(400).send('Missing resources');
    }

    const checkEmail = await User.findOne({ email });

    if (checkEmail && checkEmail._id.toString() !== id) {
      return res.status(400).send('Email already in use');
    }

    const user = await User.updateOne({ _id: id }, {
      email, name, role, picture,
    });

    if (user.nModified !== 1) {
      return res.status(400).send('Update failed');
    }

    res.send();
  } catch (error) {
    res.status(500).send('Internal Server Error');
  }
};

export const inactivate = async (req, res) => {
  try {
    const { id } = req.params;

    if (!id) {
      return res.status(400).send('Missing resources');
    }

    const user = await User.findOne({ _id: id });

    if (!user || !user.isActive) {
      return res.status(400).send('User not found or already inactive');
    }

    const movieIds = user.votes.map((v) => v.movieId);

    const movies = await Movie.find({ _id: { $in: movieIds } });

    if (movies.length > 0) {
      for (const movie of movies) {
        const movieIndex = user.votes.findIndex((v) => v.movieId.toString() === movie._id.toString());
        console.log(movie.votes);
        movie.votes -= user.votes[movieIndex].value;
        movie.totalUsers -= 1;
        console.log(movie.votes);

        await Movie.updateOne({ _id: movie._id }, { votes: movie.votes, totalUsers: movie.totalUsers });
      }
    }

    user.isActive = false;

    await User.updateOne({ _id: user._id }, user);

    res.send();
  } catch (error) {
    console.log(error);
    res.status(500).send('Internal Server Error');
  }
};

export const activate = async (req, res) => {
  try {
    const { id } = req.params;

    if (!id) {
      return res.status(400).send('Missing resources');
    }

    const user = await User.findOne({ _id: id });

    if (!user || user.isActive) {
      return res.status(400).send('User not found or already active');
    }

    const movieIds = user.votes.map((v) => v.movieId);

    const movies = await Movie.find({ _id: { $in: movieIds } });

    if (movies.length > 0) {
      for (const movie of movies) {
        const movieIndex = user.votes.findIndex((v) => v.movieId.toString() === movie._id.toString());
        movie.votes += user.votes[movieIndex].value;
        movie.totalUsers += 1;

        await Movie.updateOne({ _id: movie._id }, { votes: movie.votes, totalUsers: movie.totalUsers });
      }
    }

    user.isActive = true;

    await User.updateOne({ _id: user._id }, user);
    res.send();
  } catch (error) {
    console.log({ error });
    res.status(500).send('Internal Server Error');
  }
};
