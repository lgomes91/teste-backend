import Movie from '../models/movie.model';
import User from '../models/user.model';

export const createMovie = async (req, res) => {
  try {
    const {
      name, director, actors, genre, details,
    } = req.body;

    if (!name || !director || !actors || !genre || !details) {
      return res.status(400).send('Missing resources');
    }

    const movie = await Movie.create({
      name, director, actors, genre, details,
    });

    res.send(movie);
  } catch (error) {
    res.status(500).send('Internal Server Error');
  }
};

export const update = async (req, res) => {
  try {
    const { id } = req.params;

    const {
      name, director, actors, genre, details,
    } = req.body;

    if (!id || !name || !director || !actors || !genre || !details) {
      return res.status(400).send('Missing resources');
    }

    const updated = await Movie.updateOne({ _id: id }, {
      name, director, actors, genre, details,
    });

    if (updated.nModified !== 1) {
      return res.status(400).send('Failed to update');
    }

    res.send();
  } catch (error) {
    res.status(500).send('Internal Server Error');
  }
};

export const list = async (req, res) => {
  try {
    if (!req.query.limit || !req.query.page) {
      return res.status(400).send('Missing resources');
    }

    const {
      name, director, actors, genre,
    } = req.query;

    const { user } = req.body;

    const limit = Number(req.query.limit);
    const page = Number(req.query.page) - 1;

    const query = {};

    if (user.role === 'user') {
      query.isActive = true;
    }

    if (name) {
      query.name = { $regex: name, $options: 'si' };
    }

    if (director) {
      query.director = { $regex: director, $options: 'si' };
    }

    if (actors) {
      query.actors = { $exists: actors };
    }

    if (genre) {
      query.genre = genre;
    }

    const movies = await Movie.find(query)
      .skip(limit * page)
      .limit(limit)
      .sort({ name: 1 });

    const totalMovies = await Movie.find(query).countDocuments();

    const totalPages = (totalMovies % limit) > 0
      ? Math.trunc(totalMovies / limit) + 1
      : Math.trunc(totalMovies / limit);

    res.send({ movies, totalPages, totalMovies });
  } catch (error) {
    res.status(500).send('Internal Server Error');
  }
};

export const listById = async (req, res) => {
  try {
    const { id } = req.params;

    if (!id) {
      return res.status(400).send('Missing resources');
    }

    const movie = await Movie.findOne({ _id: id });

    if (!movie) {
      return res.status(404).send('Movie not found');
    }

    res.send(movie);
  } catch (error) {
    res.status(500).send('Internal Server Error');
  }
};

export const vote = async (req, res) => {
  try {
    const { userVote, movieId, user } = req.body;

    if (!user || !userVote || !movieId) {
      return res.status(400).send('Missing resources');
    }

    if (userVote < 0 || userVote > 4) {
      return res.status(400).send('Vote out of range');
    }

    const dbUser = await User.findById(user.id);

    const movie = await Movie.findById(movieId);

    if (!dbUser || !movie || !movie.isActive) {
      return res.status(404).send('Could not find user or movie in DB');
    }

    const hasVoted = dbUser.votes.findIndex((v) => v.movieId.toString() === movieId);

    if (hasVoted > -1) {
      if (dbUser.votes[hasVoted].value !== userVote) {
        movie.votes = movie.votes + userVote - dbUser.votes[hasVoted].value;
        dbUser.votes[hasVoted].value = userVote;
      } else {
        return res.status(202).send();
      }
    } else {
      movie.totalUsers += 1;
      movie.votes += userVote;

      dbUser.votes.push({
        movieId,
        value: userVote,
      });
    }

    const updateUser = await User.updateOne({ _id: dbUser._id }, { votes: dbUser.votes });

    if (updateUser.nModified === 0) {
      return res.status(400).send('Faield to vote');
    }

    const updateMovie = await Movie.updateOne({ _id: movieId },
      {
        totalUsers: movie.totalUsers,
        votes: movie.votes,
      });

    if (updateMovie.nModified === 0) {
      return res.status(400).send('Vote stored, but faield to computate');
    }

    res.send({
      name: movie.name,
      totalUsers: movie.totalUsers,
      votes: movie.votes,
    });
  } catch (error) {
    res.status(500).send('Internal Server Error');
  }
};

export const inactivate = async (req, res) => {
  try {
    const { id } = req.params;

    if (!id) {
      return res.status(400).send('Missing resources');
    }

    const updateMovie = await Movie.updateOne({ _id: id }, { isActive: false });

    if (updateMovie.nModified === 0) {
      return res.status(400).send('Failed to delete de movie');
    }

    res.send();
  } catch (error) {
    console.log(error);
    res.status(500).send('Internal Server Error');
  }
};
