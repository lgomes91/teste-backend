import express from 'express';
import cors from 'cors';
import { config } from 'dotenv';
import router from './routes';

import { conn } from './database';

config('../.env');
conn(process.env.MONGODB_URL);

const server = express();

server.use(cors());
server.use(express.json());
server.use(router);

server.listen(3000, () => {
  console.log('Server started on http://localhost:3000');
});
