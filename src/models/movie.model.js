import mongoose from '../database';

const movieSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
  },
  director: {
    type: String,
    required: true,
  },
  actors: {
    type: [String],
    required: true,
  },
  genre: {
    type: String,
    required: true,
  },
  votes: {
    type: Number,
    default: 0,
  },
  totalUsers: {
    type: Number,
    default: 0,
  },
  details: {
    type: String,
  },
  isActive: {
    type: Boolean,
    default: true,
  },

}, {
  timestamps: true,
});

const model = mongoose.model('Movie', movieSchema);

export const { schema } = model;
export default model;
