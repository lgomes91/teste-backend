import mongoose from '../database';

const roles = ['user', 'admin'];

const userSchema = new mongoose.Schema({
  email: {
    type: String,
    match: /^\S+@\S+\.\S+$/,
    required: true,
    unique: true,
    trim: true,
    lowercase: true,
  },
  password: {
    type: String,
    required: true,
    minlength: 6,
  },
  name: {
    type: String,
    index: true,
    trim: true,
  },
  role: {
    type: String,
    enum: roles,
    default: 'user',
  },
  picture: {
    type: String,
    trim: true,
    default: null,
  },
  isActive: {
    type: Boolean,
    default: true,
  },
  votes: [
    {
      movieId: {
        type: mongoose.Schema.ObjectId,
        ref: 'Movie',
      },
      value: Number,
    },
  ],
}, {
  timestamps: true,
});

const model = mongoose.model('User', userSchema);

export const { schema } = model;
export default model;
